/*
 * k_modes.cpp
 *
 *  Created on: Jul 12, 2017
 *      Author: Administrator
 * 	Aug 14:
 * 		- k_modes_GRD(): use GRD_dist()
 *
 */

#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>

#include "helper.h"
#include "k_modes.h"

using namespace std;


///////////////////
int main(int argc, char* args[]) {

	int N = 47;
	int M = 35;
	int K = 4;
	int T = 20;
	int n_runs = 100;
	string path = "data/";
	string file_name = "soybean-small.data";

	// COMMAND-LINE <N> <M> <K> <T> <n_runs> <file_name>
	cout<<"k-modes-standard - C++\n";
	if(argc > 1)
		N = stoi(args[1]);
	if(argc > 2)
		M = stoi(args[2]);
	if(argc > 3)
		K = stoi(args[3]);
	if(argc > 4)
		T = stoi(args[4]);
	if(argc > 5)
		n_runs = stoi(args[5]);
	if(argc > 6)
		file_name = string(args[6]);
	cout<<"N = "<<N<<endl;
	cout<<"M = "<<M<<endl;
	cout<<"K = "<<K<<endl;
	cout<<"#iterations = "<<T<<endl;
	cout<<"n_runs = "<<n_runs<<endl;
	cout<<"file_name = "<<file_name<<endl;

	//
//	int** X = load_data(path, file_name, N, M);
	int** X = load_data_arff(path, file_name, N, M);
	int* GT = true_membership(X, N, M);
//	print_data(X, N, M);

	__int64 start = Timer::get_millisec();

	double avgRI = 0.0;
	double avgAcc = 0.0;
	double avgPrec = 0.0;
	double avgRecall = 0.0;
	int** C = new int*[K];
	for (int i = 0; i < K; i++)
		C[i] = new int[K];

	for (int i = 0; i < n_runs; i++){
		int** Q = init_modes_random(X, N, M, K);
//		cout<<"Q:"<<endl;
//		print_data(Q, K, M);

		//
		int* W = k_modes(X, Q, N, M, K, T);
//		int* W = k_modes_GRD(X, Q, N, M, K, T);

		avgRI += metric_rand_index_fast(GT, W, N, K);
//		avgAcc += metric_accuracy(GT, W, N, K);

		metric_confusion_matrix(GT, W, N, K, C, false);
		vector<double> ret = metric_accuracy_precision_recall(C, N, K);
		avgAcc += ret[0];
		avgPrec += ret[1];
		avgRecall += ret[2];
	}
	cout<<"Avg. Rand Index(GT,W) = "<<avgRI/n_runs<<endl;
	cout<<"Avg. Accuracy(GT,W) = "<<avgAcc/n_runs<<endl;
	cout<<"Avg. Precesion(GT,W) = "<<avgPrec/n_runs<<endl;
	cout<<"Avg. Recall(GT,W) = "<<avgRecall/n_runs<<endl;
	cout<<"k_modes - DONE, elapsed : "<< (Timer::get_millisec() - start) <<endl;

	//
	return 0;
}


