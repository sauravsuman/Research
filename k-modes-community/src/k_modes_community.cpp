/*
 * k_modes_community.cpp
 *
 *  Created on: Jul 10, 2017
 *      Author: Administrator
 *      - convert from Python (private-cluster/k-modes-community.py)
 *  Aug 3:
 *  	- build_graph_FQA()
 *  	- build_graph_approx()
 *  	- analyze_membership(): sort clusters in the membership vector
 *  Aug 6:
 *  	- rename find_clusters() -> find_clusters_community()
 *  	- find_clusters_connected(): binary search
 *  Aug 14:
 *  	- analyze_membership(): updated to include k-modes iterations --> better results on mushroom but NOT on breast and dermatology
 *  	- build_graph_approx_with_K():
 *  	- build_graph_with_K()
 *  Oct 30:
 *  	- fix analyze_membership() for ret.size() < topK
 *  	- build_graph_fixed_with_K(): deterministic
 */

#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <vector>
#include <map>
#include <random>       // std::default_random_engine

#include "helper.h"
#include "k_modes.h"

#include <igraph/igraph.h>

using namespace std;

//// for each node, pick num_sample other nodes and check the distance against an edge_threshold
igraph_t build_graph_approx(int** X, int N, int M, int num_sample, int edge_threshold){
	igraph_t g;
	igraph_vector_t edges;

	igraph_vector_init(&edges, 0);

	default_random_engine gen;
	uniform_int_distribution<int> distr(0, N-1);

	int v;
	for (int u = 0; u < N; u++)
		for (int i = 0; i < num_sample; i++){
			v = distr(gen);
			if (dist(X[u], X[v], M) <= edge_threshold){		// u < v &&
				igraph_vector_push_back(&edges, u);
				igraph_vector_push_back(&edges, v);
			}
		}

	igraph_create(&g, &edges, N, 0);
	igraph_simplify(&g, 1, 0, NULL);	// remove duplicate edges

	cout<<"#nodes = "<<igraph_vcount(&g)<<endl;
	cout<<"#edges = "<<igraph_ecount(&g)<<endl;

	// connected components
	int num_cluster;
	igraph_clusters(&g, NULL, NULL, &num_cluster, IGRAPH_WEAK);
	cout<<"#components = "<<num_cluster<<endl;

	//
	return g;
}

//// for each node, pick num_sample other nodes and check the distance against edge_threshold
igraph_t build_graph_approx_with_K(int** X, int N, int M, int num_sample, int K){
	igraph_t g;
	igraph_vector_t edges;

	igraph_vector_init(&edges, 0);

	default_random_engine gen;
	uniform_int_distribution<int> distr(0, N-1);

	// 1 - find edge_threshold by K
	int v;
	double D[M+1];		// distance CDF
	for (int d = 0; d <= M; d++)
		D[d] = 0.0;
	for (int u = 0; u < N; u++)
		for (int i = 0; i < num_sample; i++){
			v = distr(gen);
			D[dist(X[u], X[v], M)] += 1;
		}
	for (int d = 1; d <= M; d++)
		D[d] = D[d] + D[d-1];
	for (int d = 0; d <= M; d++)
		D[d] = D[d]/D[M];

	int edge_threshold = 0;
	double invK = 1.0/K;
	for (int d = 0; d < M; d++)
		if (D[d+1] > invK){
			edge_threshold = d;
			break;
		}
	cout<<"computed edge_threshold = "<<edge_threshold<<endl;

	// 2 - build graph
	for (int u = 0; u < N; u++)
		for (int i = 0; i < num_sample; i++){
			v = distr(gen);
			if (dist(X[u], X[v], M) <= edge_threshold){		// u < v &&
				igraph_vector_push_back(&edges, u);
				igraph_vector_push_back(&edges, v);
			}
		}

	igraph_create(&g, &edges, N, 0);
	igraph_simplify(&g, 1, 0, NULL);	// remove duplicate edges

	cout<<"#nodes = "<<igraph_vcount(&g)<<endl;
	cout<<"#edges = "<<igraph_ecount(&g)<<endl;

	// connected components
	int num_cluster;
	igraph_clusters(&g, NULL, NULL, &num_cluster, IGRAPH_WEAK);
	cout<<"#components = "<<num_cluster<<endl;

	//
	return g;
}

//// deterministic: sort X by attributes
igraph_t build_graph_fixed_with_K(int** X, int N, int M, int num_sample, int K){
	igraph_t g;
	igraph_vector_t edges;

	igraph_vector_init(&edges, 0);

	// index sort X (see gen_FQArr())
	vector<int> idx(N);
	iota(idx.begin(), idx.end(), 0);

	auto comparator = [X, M](int a, int b){
		return compare(X, a, b, M);
	};

	sort(idx.begin(), idx.end(), comparator);

	// 2 - find edge_threshold by K
	int v;
	double D[M+1];		// distance CDF
	for (int d = 0; d <= M; d++)
		D[d] = 0.0;
	for (int u = 0; u < N; u++)
		for (int i = 0; i < num_sample; i++){
			v = idx[(u + i) % N];
			D[dist(X[u], X[v], M)] += 1;
		}
	for (int d = 1; d <= M; d++)
		D[d] = D[d] + D[d-1];
	for (int d = 0; d <= M; d++)
		D[d] = D[d]/D[M];

	int edge_threshold = 0;
	double invK = 1.0/K;
	for (int d = 0; d < M; d++)
		if (D[d+1] > invK){
			edge_threshold = d;
			break;
		}
	cout<<"computed edge_threshold = "<<edge_threshold<<endl;

	// 3 - build graph
	for (int u = 0; u < N; u++)
		for (int i = 0; i < num_sample; i++){
			v = idx[(u + i) % N];
			if (dist(X[u], X[v], M) <= edge_threshold){		// u < v &&
				igraph_vector_push_back(&edges, u);
				igraph_vector_push_back(&edges, v);
			}
		}

	igraph_create(&g, &edges, N, 0);
	igraph_simplify(&g, 1, 0, NULL);	// remove duplicate edges

	cout<<"#nodes = "<<igraph_vcount(&g)<<endl;
	cout<<"#edges = "<<igraph_ecount(&g)<<endl;

	// connected components
	int num_cluster;
	igraph_clusters(&g, NULL, NULL, &num_cluster, IGRAPH_WEAK);
	cout<<"#components = "<<num_cluster<<endl;

	//
	return g;
}

////
igraph_t build_graph(int** X, int N, int M, int edge_threshold){
	igraph_t g;
	igraph_vector_t edges;

	igraph_vector_init(&edges, 0);

	for (int u = 0; u < N; u++)
		for (int v = u+1; v < N; v++)
			if (dist(X[u], X[v], M) <= edge_threshold){
				igraph_vector_push_back(&edges, u);
				igraph_vector_push_back(&edges, v);
			}

	igraph_create(&g, &edges, N, 0);
	cout<<"#nodes = "<<igraph_vcount(&g)<<endl;
	cout<<"#edges = "<<igraph_ecount(&g)<<endl;

	// connected components
//	igraph_vector_t membership, csize;
//	int num_cluster;
//	igraph_vector_init(&membership, 0);
//	igraph_vector_init(&csize, 0);
//
//	igraph_clusters(&g, &membership, &csize, &num_cluster, IGRAPH_WEAK);

	int num_cluster;
	igraph_clusters(&g, NULL, NULL, &num_cluster, IGRAPH_WEAK);
	cout<<"#components = "<<num_cluster<<endl;

	//
	return g;
}

////
igraph_t build_graph_with_K(int** X, int N, int M, int K){
	igraph_t g;
	igraph_vector_t edges;

	igraph_vector_init(&edges, 0);

	// 1 -
	double D[M+1];		// distance CDF
	for (int d = 0; d <= M; d++)
		D[d] = 0.0;
	for (int u = 0; u < N; u++)
		for (int v = u+1; v < N; v++)
			D[dist(X[u], X[v], M)] += 1;
	cout<<"D = "<<D[0]<<" ";
	for (int d = 1; d <= M; d++){
		D[d] = D[d] + D[d-1];
		cout<<D[d]<<" ";
	}
	cout<<endl;
	cout<<"D = ";
	for (int d = 0; d <= M; d++){
		D[d] = D[d]/D[M];
		cout<<D[d]<<" ";
	}
	cout<<endl;

	int edge_threshold = 0;
	double invK = 1.0/K;
	for (int d = 0; d < M; d++)
		if (D[d+1] > invK){
			edge_threshold = d;
			break;
		}
	cout<<"computed edge_threshold = "<<edge_threshold<<endl;

	// 2 -
	for (int u = 0; u < N; u++)
		for (int v = u+1; v < N; v++)
			if (dist(X[u], X[v], M) <= edge_threshold){
				igraph_vector_push_back(&edges, u);
				igraph_vector_push_back(&edges, v);
			}

	igraph_create(&g, &edges, N, 0);
	cout<<"#nodes = "<<igraph_vcount(&g)<<endl;
	cout<<"#edges = "<<igraph_ecount(&g)<<endl;

	// connected components
//	igraph_vector_t membership, csize;
//	int num_cluster;
//	igraph_vector_init(&membership, 0);
//	igraph_vector_init(&csize, 0);
//
//	igraph_clusters(&g, &membership, &csize, &num_cluster, IGRAPH_WEAK);

	int num_cluster;
	igraph_clusters(&g, NULL, NULL, &num_cluster, IGRAPH_WEAK);
	cout<<"#components = "<<num_cluster<<endl;

	//
	return g;
}

//// keep topK clusters and reassign remaining items
int* analyze_membership(int** X, int N, int M, igraph_vector_t *membership, int topK){
	vector<pair_int> ret;
	cout<<"topK = "<<topK<<endl;

	map<int, int> pairs;
	for (long int i = 0; i < N; i++) {
		int val = VECTOR(*membership)[i];
		if (pairs.count(val) == 0)
			pairs[val] = 1;
		else
			pairs[val] += 1;
	}

	// copy pairs to ret and sort descending by size
	for (map<int, int>::iterator it = pairs.begin(); it != pairs.end(); it++)
		ret.push_back(pair_int(it->first, it->second));
	sort(ret.begin(), ret.end(), [](const pair_int &x, const pair_int &y){return x.val > y.val;} );

//	for (int c = 0; c < ret.size(); c++)
//		cout<<ret[c].key<<" : "<<ret[c].val<<endl;

	//
	cout<<"#communities = "<<ret.size()<<endl;
	if (ret.size() < topK){
		int pad = topK - ret.size();
		for (int k = 0; k < pad; k++)
			ret.push_back(pair_int(-1, 0));
	}

	// keep topK clusters, compute topK modes and reassign remaining items
	int* W = new int[N];
	for (long int i=0; i < N; i++)
		W[i] = VECTOR(*membership)[i];

	int** Q = new int*[topK];
	for (int i=0; i < topK; i++)
		Q[i] = new int[M+1];

	cout<<"top K clusters\n";
	for (int c = 0; c < topK; c++){
		cout<<ret[c].key<<" : "<<ret[c].val<<endl;

		int cid = ret[c].key;

		vector<int> idx;
		for (int i = 0; i < N; i++)
			if (W[i] == cid)
				idx.push_back(i);
		//
		Q[c] = find_mode(X, idx, N, M);
		Q[c][M] = cid;
	}

	// WAY - 1
	// reassign remaining items to nearest mode
	for (int c = topK; c < ret.size(); c++){
		int cid = ret[c].key;
		for (int i = 0; i < N; i++)
			if (W[i] == cid){
				int min_cid = 0;
				for (int k = 1; k < topK; k++)
					if (dist(X[i], Q[min_cid], M) > dist(X[i], Q[k], M))
						min_cid = k;
				W[i] = ret[min_cid].key;
			}
	}
	// reassign cluster ids for metric_accuracy() in k_modes.h
	map<int, int> idx;
	int id = 0;
	for (int i = 0; i < N; i++)
		if (idx.count(W[i]) == 0){
			idx[W[i]] = id;
			id++;
		}
	// DEBUG
//	cout<<"idx\n";
//	for (map<int, int>::iterator it = idx.begin(); it != idx.end(); it++)
//		cout<<it->first<<" "<<it->second<<endl;

	//
	for (int i = 0; i < N; i++)
		W[i] = idx[W[i]];

	cout<<"cluster_cost = "<<cluster_cost(X, Q, W, N, M)<<endl;

	// WAY 2 - use Q as initial modes in k_modes()
//	W = k_modes(X, Q, N, M, topK, 5);

	//
//	return ret;
	return W;
}

//// for Louvain method
void show_clusters(igraph_t *g, igraph_vector_t *membership, igraph_matrix_t *memberships, igraph_vector_t *modularity, FILE* f) {
	long int i, j, no_of_nodes = igraph_vcount(g);

	j = igraph_vector_which_max(modularity);
	for (i = 0; i < igraph_vector_size(membership); i++) {
		if (VECTOR(*membership)[i] != MATRIX(*memberships, j, i)) {
			fprintf(f, "WARNING: best membership vector element %li does not match the best one in the membership matrix\n", i);
		}
	}

	fprintf(f, "Modularities:\n");
	igraph_vector_print(modularity);

	// display top-level
	igraph_vector_print(membership);

	// display multi-level
//	for (i = 0; i < igraph_matrix_nrow(memberships); i++) {
//		for (j = 0; j < no_of_nodes; j++) {
//			fprintf(f, "%ld ", (long int) MATRIX(*memberships, i, j));
//		}
//		fprintf(f, "\n");
//	}

	fprintf(f, "\n");
}

//// for InfoMap
void show_clusters(igraph_t *g, igraph_vector_t *membership, igraph_real_t *codelength, FILE* f) {

	fprintf(f, "Membership:\n");
	igraph_vector_print(membership);

	fprintf(f, "codelength = %.2f\n", *codelength);

	igraph_real_t modularity;
	igraph_modularity(g, membership, &modularity, 0);
	fprintf(f, "modularity = %.2f\n", modularity);

	fprintf(f, "\n");
}

////
int* find_clusters_community(igraph_t *g, int** X, int N, int M, int K){
	igraph_vector_t modularity, membership;
	igraph_matrix_t memberships;
	igraph_real_t codelength;

	igraph_vector_init(&modularity, 0);
	igraph_vector_init(&membership, 0);
	igraph_matrix_init(&memberships, 0, 0);

	// Blondel et al.
//	if (algo_type == 0){
		__int64 start = Timer::get_millisec();
		igraph_community_multilevel(g, 0, &membership, &memberships, &modularity);
//		show_clusters(g, &membership, &memberships, &modularity, stdout);
		cout<<"#levels = "<<igraph_matrix_nrow(&memberships)<<endl;

		cout<<"igraph_community_multilevel - DONE, elapsed : "<< (Timer::get_millisec() - start) <<endl;

		start = Timer::get_millisec();
		int* W = analyze_membership(X, N, M, &membership, K);
		cout<<"analyze_membership - DONE, elapsed : "<< (Timer::get_millisec() - start) <<endl;

//	}

	// InfoMap
////	if (algo_type == 1){
//		__int64 start = Timer::get_millisec();
//		igraph_community_infomap(g, 0, 0, 5, &membership, &codelength);
////		show_clusters(g, &membership, &codelength, stdout);
//		cout<<"igraph_community_infomap - DONE, elapsed : "<< (Timer::get_millisec() - start) <<endl;
//
//		start = Timer::get_millisec();
//		int* W = analyze_membership(X, N, M, &membership, K);
//		cout<<"analyze_membership - DONE, elapsed : "<< (Timer::get_millisec() - start) <<endl;
//
////	}

	//
	return W;
}

//// binary search on r (edge threshold)
//void find_clusters_connected(int** X, int N, int M, int K, int num_sample){
//
//
//	list<wEdge> weighted_edges;
//
//	// 1 - compute N*num_sample distances d(u,v)
//	default_random_engine gen;
//	uniform_int_distribution<int> distr(0, N-1);
//	int v;
//	for (int u = 0; u < N; u++)
//		for (int i = 0; i < num_sample; i++){
//			v = distr(gen);
//			if (u < v)
//				weighted_edges.push_back(wEdge(u, v, dist(X[u], X[v], M)));
//		}
//	cout<<"#weighted edges = "<<weighted_edges.size()<<endl;
//
//	// 2 - binary search
//	int lo = 0;
//	int hi = M;
//	int r = (lo + hi)/2;
////	while (true){
//	for (r = 1; r <= M; r++){
//		cout<<"r = "<<r<<endl;
//
//		// build graph g
//		igraph_t g;
//		igraph_vector_t edges;
//
//		igraph_vector_init(&edges, 0);
//
//		for (wEdge item : weighted_edges)
//			if (item.weight <= r){
//				igraph_vector_push_back(&edges, item.u);
//				igraph_vector_push_back(&edges, item.v);
//			}
//		igraph_create(&g, &edges, N, 0);
//		cout<<"#nodes = "<<igraph_vcount(&g)<<endl;
//		cout<<"#edges = "<<igraph_ecount(&g)<<endl;
//
//		// connected components
//		igraph_vector_t membership;
//		igraph_vector_init(&membership, 0);
//
//		int num_cluster;
//		igraph_clusters(&g, &membership, NULL, &num_cluster, IGRAPH_WEAK);
//		cout<<"#components = "<<num_cluster<<endl;
//
//		// sort clusters by size (descending)
//		vector<pair_int> ret = analyze_membership(&membership);
//
//		double logN = log(N)/log(2.0);
//		int num_lc = 0;		// #large clusters (>= logN)
//		for (pair_int p : ret)
//			if (p.val >= logN){
//				cout<<p.key<<" : "<<p.val<<endl;
//				num_lc += 1;
//			}
//		cout<<"num_lc = "<<num_lc<<endl;
//	}
//
//}

///////////////////
int main(int argc, char* args[]) {

	int N = 47;
	int M = 21;
	int K = 4;
	int edge_threshold = 5;
//	int algo_type = 0;		// Louvain (0) or InfoMap (1)
	int full_graph = 0;		// 1: full edge matrix, 0: sampled edge matrix
	string path = "data/";
	string file_name = "soybean-small-mod.data";
	int P = 4;

	// COMMAND-LINE <N> <M> <K> <full_graph> <edge_threshold> <file_name>
	cout<<"k-modes-community - C++\n";
	if(argc > 1)
		N = stoi(args[1]);
	if(argc > 2)
		M = stoi(args[2]);
	if(argc > 3)
		K = stoi(args[3]);
	if(argc > 4)
		full_graph = stoi(args[4]);
	if(argc > 5)
		edge_threshold = stoi(args[5]);
	if(argc > 6)
		file_name = string(args[6]);
	cout<<"N = "<<N<<endl;
	cout<<"M = "<<M<<endl;
	cout<<"K = "<<K<<endl;
	cout<<"full_graph = "<<full_graph<<endl;
	cout<<"edge_threshold = "<<edge_threshold<<endl;
	cout<<"file_name = "<<file_name<<endl;
//	cout<<"P = "<<P<<endl;

	//
//	int** X = load_data(path, file_name, N, M);
	int** X = load_data_arff(path, file_name, N, M);
	int* GT = true_membership(X, N, M);
//	print_data(X, N, M);

//	int N = 12960;
//	int M = 8;
//	int** X = load_data("data/", "nursery-converted.data", N, M);



	// TEST dist()
//	cout<<"test dist"<<endl;
//	cout<<dist(X[0],X[0],M)<<endl;
//	cout<<dist(X[0],X[1],M)<<endl;
//	cout<<dist(X[3],X[4],M)<<endl;

	// TEST build_graph()
//	igraph_t g = build_graph(X, N, M, edge_threshold);
//	find_clusters_community(&g, algo_type);

	// TEST build_graph_FQA
//	igraph_t g = build_graph_FQA(X, N, M, P, edge_threshold);

	__int64 start = Timer::get_millisec();
	__int64 all_start = Timer::get_millisec();
	igraph_t g;
	if (full_graph == 1){
//		g = build_graph(X, N, M, edge_threshold);
		g = build_graph_with_K(X, N, M, K);

	}else{
//		int num_sample = (int)sqrt(N); //2 * (int)sqrt(N);

		int num_sample = stoi(args[5]);						// 5-th param used for num_sample
		cout<<"num_sample = "<<num_sample<<endl;

//		g = build_graph_approx(X, N, M, num_sample, edge_threshold);
//		g = build_graph_approx_with_K(X, N, M, num_sample, K);
		g = build_graph_fixed_with_K(X, N, M, num_sample, K);

	}
	cout<<"build_graph - DONE, elapsed : "<< (Timer::get_millisec() - start) <<endl;

	start = Timer::get_millisec();
	int* W = find_clusters_community(&g, X, N, M, K);
//	find_clusters_connected(X, N, M, num_sample);
	cout<<"find_clusters_community - DONE, elapsed : "<< (Timer::get_millisec() - start) <<endl;

	start = Timer::get_millisec();
	cout<<"Rand Index(GT,W) = "<<metric_rand_index_fast(GT, W, N, K)<<endl;
	int** C = new int*[K];
	for (int i = 0; i < K; i++)
		C[i] = new int[K];
	metric_confusion_matrix(GT, W, N, K, C, true);
	print_matrix(C, K);
	vector<double> ret = metric_accuracy_precision_recall(C, N, K);
	cout<<"Accuracy = "<<ret[0]<<endl;
	cout<<"Precision = "<<ret[1]<<endl;
	cout<<"Recall = "<<ret[2]<<endl;
	cout<<"metrics, elapsed : "<< (Timer::get_millisec() - start) <<endl;
	//
//	find_best_permutation(GT, W, N, K);


	cout<<"k_modes_community - DONE, elapsed : "<< (Timer::get_millisec() - all_start) <<endl;

	//
	return 0;
}

