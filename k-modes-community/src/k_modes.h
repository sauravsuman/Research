/*
 * k_modes.h
 *
 *  Created on: Jul 12, 2017
 *      Author: Administrator
 *      - common functions for k-modes clustering
 *  Aug 5:
 *  	- init_modes_max_min() (paper: A new initialization method for categorical data clustering, 2009)
 *  	- print_stats(): compute the average distance for each cluster
 *  Aug 7:
 *  	- distance_distr():	distance distribution (plotted in MATLAB)
 *  	- metric_rand_index() (paper: A Global-Relationship Dissimilarity Measure for the k-Modes Clustering Algorithm)
 *  	- metric_accuracy() (call Hungarian algo)
 *  Aug 14:
 *  	- GRD_dist(): (paper: A Global-Relationship Dissimilarity Measure for the k-Modes Clustering Algorithm)
 *  Oct 25:
 *  	- confusion_matrix_by_permutation(), find_best_permutation()
 *  Oct 28:
 *  	- load_data_arff()
 *  Oct 31:
 *  	- modularity_and_cluster_cost(): to compare against the ground-truth
 *  Nov 1:
 *  	- init_ground_truth_distinct()
 *  Nov 3:
 *  	- find_nearest_cluster_GT(): find nearest ground-truth cluster-id
 */

#ifndef K_MODES_H_
#define K_MODES_H_

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

#include "hungarian.h"

using namespace std;

// used in init_modes_freq()
struct pair_int{
	int key;
	int val;
	////
	pair_int(int _key, int _val){
		key = _key;
		val = _val;
	}

	bool operator < (const pair_int& arg0) const{
		return (this->val > arg0.val);
	}
};

struct range{
	int start;
	int end;
	////
	range(int _start, int _end){
		start = _start;
		end = _end;
	}
};

struct wEdge{
	int u;
	int v;
	int weight;

	wEdge(int _u, int _v, int _weight){
		u = _u;
		v = _v;
		weight = _weight;
	}
};


///////////

////
int** load_data(string path, string file_name, int N, int M){

	// init
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];

	//
	ifstream f(path + file_name);
	if(!f) {
		cout << "Cannot open file.\n";
		return NULL;
	}

	//
	string line;
	int i = 0;
	while (getline(f, line)){
		vector<string> items = Formatter::split(line, ',');

		for (int j = 0; j < M; j++)
			X[i][j] = stoi(items[j]);
		X[i][M] = stoi(items[M]);

		i++;
	}
	f.close();

	//
	return X;
}

////
int** load_data_arff(string path, string file_name, int N, int M){

	// init
	int** X = new int*[N];
	for (int i = 0; i < N; i++)
		X[i] = new int[M+1];

	//
	ifstream f(path + file_name);
	if(!f) {
		cout << "Cannot open file.\n";
		return NULL;
	}

	//
	string line;
	int i = 0;
	while (getline(f, line)){
		if (line[0] == '@')
			continue;

		vector<string> items = Formatter::split(line, ',');

		for (int j = 0; j < M; j++)
			X[i][j] = stoi(items[j].substr(1, items[j].size()-2));
		X[i][M] = stoi(items[M].substr(1, items[M].size()-2));

		i++;
	}
	f.close();

	//
	return X;
}


////
int* true_membership(int** X, int N, int M){
	int* T = new int[N];
	for (int i = 0; i < N; i++)
		T[i] = X[i][M];

	return T;
}

////
void print_array(int** X, int N, int M){
	for (int i = 0; i < N; i++){
		for (int j = 0; j < M; j++)
			cout<<X[i][j]<<" ";
		cout<<endl;
	}
}

////
void print_list(int* X, int M){
	for (int j = 0; j < M; j++)
		cout<<setw(2)<<X[j]<<" ";
	cout<<endl;
}

////
void print_array_by_index(int** X, vector<int> idx, int N, int M){
	for (int i = 0; i < N; i++){
		for (int j = 0; j < M; j++)
			cout<<X[idx[i]][j]<<" ";
		cout<<endl;
	}
}

////
void print_clusters(vector<vector<int>> clusters){
	cout<<"clusters:"<<endl;
	for (vector<int> cluster : clusters){
		for (int id : cluster)
			cout<<id<<" ";
		cout<<endl;
	}
}


////
void print_data(int** X, int N, int M){
	for (int i = 0; i < N; i++){
		for (int j = 0; j < M; j++)
			cout<<X[i][j]<<",";
		cout<<X[i][M]<<endl;
	}
}

//// Hamming distance
int dist(int* x, int* y, int M){
	int ret = 0;
	for (int j = 0; j < M; j++)
		if (x[j] != y[j])
			ret ++;
	return ret;
}

////
void compute_and_print_dist(int** X, int** Q, int N, int M, int K){
	for (int k = 0; k < K; k++){
		for (int i = 0; i < N; i++)
			cout<<setw(2)<< dist(Q[k], X[i], M)<<" ";
		cout<<endl;
	}
}

//// GRD distance between mode z and point x
double GRD_dist(int** Q, int* z, int* x, int M, int K){
	double ret = M;
	for (int j = 0; j < M; j++)
		if (z[j] == x[j]){
			int S = 0;
			for (int k = 0; k < K; k++)
				if (Q[k][j] == x[j])
					S ++;
			ret  = ret - (1 - double(S-1)/K);
		}
	return ret;
}

////
void shuffle_idx(vector<int> &idx){
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	shuffle(idx.begin(), idx.end(), default_random_engine(seed));
}

//// random initialization
int** init_modes_random(int** X, int N, int M, int K){
	int** Q = new int*[K];
	for (int k = 0; k < K; k++)
		Q[k] = new int[M+1];

	// shuffle
	vector<int> idx;
	for (int i = 0; i < N; i++)
		idx.push_back(i);

	shuffle_idx(idx);

	// copy
    for (int k = 0; k < K; k++)
    	for (int j = 0; j < M; j++)
    		Q[k][j] = X[idx[k]][j];

	//
    return Q;
}

//// use ground-truth clusters to select K items from K clusters
int** init_ground_truth_distinct(int** X, int N, int M, int K){
	int** Q = new int*[K];
	for (int k = 0; k < K; k++)
		Q[k] = new int[M+1];



	// copy
    for (int k = 0; k < K; k++){
    	vector<int> idx;
    	for (int i = 0; i < N; i++)
    		if (X[i][M] == k)
    			idx.push_back(i);

    	shuffle_idx(idx);

    	for (int j = 0; j < M; j++)
    		Q[k][j] = X[idx[0]][j];		// get the first id in idx
    }

	//
    return Q;
}

//// count and sort values in each attribute
vector<vector<pair_int>> attr_stats(int** X, int N, int M){
	vector<vector<pair_int>> counter;

	// stats for each attribute
	for (int j = 0; j < M; j++){
		counter.push_back(vector<pair_int>());

		int max_val = 0;
		for (int i = 0; i < N; i++)
			if (max_val < X[i][j])
				max_val = X[i][j];
		// count
		int count[max_val+1];
		for (int t = 0; t < max_val+1; t++)
			count[t] = 0;
		for (int i = 0; i < N; i++)
			count[X[i][j]] += 1;
		//
		for (int t = 0; t < max_val+1; t++)
			if (count[t] > 0)
				counter[j].push_back(pair_int(t, count[t]));
		// sort counter
		sort(counter[j].begin(), counter[j].end());

		// debug
//		cout<<"attribute "<<j<<endl;
//		for (pair_int p : counter[j])
//			cout<<p.key<<" "<<p.val<<endl;

	}
	//
	return counter;
}

//// frequency-based initialization
int** init_modes_freq(int** X, int N, int M, int K, bool use_seed){
	int** Q = new int*[K];

	//
	vector<vector<pair_int>> counter = attr_stats(X, N, M);

	// random generator
	unsigned seed = 0;
	if (use_seed)
		seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine generator(seed);

	for (int k = 0; k < K; k++)
		Q[k] = new int[M+1];

	for (int j = 0; j < M; j++){
		uniform_int_distribution<int> distr(0, counter[j].size()-1);
		vector<int> values;
		for (pair_int p : counter[j])
			values.push_back(p.key);

		for (int k = 0; k < K; k++)
			Q[k][j] = values[distr(generator)];
	}


	//
	return Q;
}

//// average density for X[i][j], used in init_modes_max_min()
double* compute_density(int** X, int N, int M){
	double* D = new double[N];
	for (int i = 0; i < N; i++)
		D[i] = 0.0;

	// maps for attribute counting
	vector<map<int, int>> attrs;
	for (int j = 0; j < M; j++){
		attrs.push_back(map<int, int>());
		for (int i = 0; i < N; i++)
			if (attrs[j].count(X[i][j]) == 0)
				attrs[j][X[i][j]] = 1;
			else
				attrs[j][X[i][j]] += 1;

		// print
//		for (map<int,int>::iterator it = attrs[j].begin(); it != attrs[j].end(); it++)
//			cout<<it->first<<":"<<it->second<<", ";
//		cout<<endl;
	}

	//
	for (int i = 0; i < N; i++){
		double sum = 0.0;
		for (int j = 0; j < M; j++)
			sum += attrs[j][X[i][j]];
//		cout<<"i = "<<i<<" sum = "<<sum<<endl;
		D[i] = sum/M;
	}

	//
	return D;
}

////
int** init_modes_max_min(int** X, int N, int M, int K){
	int** Q = new int*[K];
	for (int k = 0; k < K; k++)
		Q[k] = new int[M+1];

	// density
	double* D = compute_density(X, N, M);

	int C[K];
	// first center
	C[0] = 0;
	for (int i = 1; i < N; i++)
		if (D[C[0]] < D[i])
			C[0] = i;
	cout<<"C[0] = "<<C[0]<<" : "<<X[C[0]][M]<<endl;		// print cluster label
	// second center
	C[1] = C[0];
	for (int i = 0; i < N; i++)
		if (D[C[1]]*dist(X[C[1]], X[C[0]], M)  < D[i]*dist(X[i], X[C[0]], M))
			C[1] = i;
	cout<<"C[1] = "<<C[1]<<" : "<<X[C[1]][M]<<endl;
	// remaining centers
	for (int k = 2; k < K; k++){
		double max = 0;

		for (int i = 0; i < N; i++){
			double min_d = 1e10;
			for (int m = 0; m < k; m++)
				if (min_d > D[i]*dist(X[i], X[C[m]], M))
					min_d = D[i]*dist(X[i], X[C[m]], M);
			//
			if (max < min_d){
				max = min_d;
				C[k] = i;
			}
		}
		cout<<"C["<<k<<"] = "<<C[k]<<" : "<<X[C[k]][M]<<endl;
	}
	//
	for (int k = 0; k < K; k++)
		for (int j = 0; j < M; j++)
			Q[k][j] = X[C[k]][j];
	//
	return Q;
}

//// data X, modes Q, membership W
int cluster_cost(int** X, int** Q, int* W, int N, int M){
    int cost = 0;
    for (int i = 0; i < N; i++)
        cost += dist(X[i], Q[W[i]], M);
    return cost;
}

////
// idx: list of object ids in current cluster
int* find_mode(int** X, vector<int> &idx, int N, int M){
    int* mode = new int[M+1];
    //
    if (idx.size() == 0){
//    	cout<<"idx.size() == 0 !\n";
		unsigned seed = chrono::system_clock::now().time_since_epoch().count();
		default_random_engine gen(seed);
		uniform_int_distribution<int> distr(0, 1000000);

    	int id = distr(gen) % N;
    	memcpy(mode, X[id], M*sizeof(int));
    	return mode;
    }

    // frequency
    for (int j = 0; j < M; j++){
    	int max_val = 0;
    	for (int i : idx)
    		if (max_val < X[i][j])
    			max_val = X[i][j];
        // count
    	int count[max_val+1];
    	for (int t = 0; t < max_val+1; t++)
    		count[t] = 0;
    	for (int i : idx)
    		count[X[i][j]] += 1;
    	// max count (minimum key)
    	int max_count = 0;
    	for (int t = 0; t < max_val+1; t++)
    		if (max_count < count[t]){
    			max_count = count[t];
    			mode[j] = t;
    		}

    }
    return mode;
}

//// T: #iterations, return membership W
int* k_modes(int** X, int** Q, int N, int M, int K, int T, bool print_cost = false){
	int* W = new int[N];


    int cur_cost = 1000000000;
//     while True:
    vector<vector<int>> clusters;

    for (int t = 0; t < T; t++){
    	clusters.clear();
    	for (int k = 0; k < K; k++)
    	    clusters.push_back(vector<int>());

        // allocate X[i] to the nearest modes Q
        for (int i = 0; i < N; i++){
            int near = 0;
            int d_near = dist(X[i], Q[0], M);
            for (int k = 1; k < K; k++){
                int d_temp = dist(X[i], Q[k], M);
                if (d_near > d_temp ){
                    near = k;
                    d_near = d_temp;
                }
            }
            W[i] = near;
            clusters[near].push_back(i);
        }
        // compute clustering cost
        if (print_cost){
			cur_cost = cluster_cost(X, Q, W, N, M);
			cout<<"cur_cost = "<<cur_cost<<endl;

//			cout<<"labels :"<<endl;
//			print_list(W, N);						// labels
//			cout<<"dist matrix :"<<endl;
//			compute_and_print_dist(X, Q, N, M, K);	// dist matrix
        }
        // update modes
        for (int k = 0; k < K; k++)
            Q[k] = find_mode(X, clusters[k], N, M);

//        if (print_cost){
//        	cout<<"Q :"<<endl;
//        	print_array(Q, K, M);
//        }
    }
    //
//    print_clusters(clusters);

    if (print_cost){
		cur_cost = cluster_cost(X, Q, W, N, M);
		cout<<"cur_cost = "<<cur_cost<<endl;
    }
    //
    return W;
}

//// T: #iterations, return membership W
int* k_modes_GRD(int** X, int** Q, int N, int M, int K, int T){
	int* W = new int[N];


    int cur_cost = 1000000000;
//     while True:
    vector<vector<int>> clusters;

    for (int t = 0; t < T; t++){
    	clusters.clear();
    	for (int k = 0; k < K; k++)
    	    clusters.push_back(vector<int>());

        // allocate X[i] to the nearest modes Q
        for (int i = 0; i < N; i++){
            int near = 0;
            double d_near = GRD_dist(Q, Q[0], X[i], M, K);			// GRD_dist()
            for (int k = 1; k < K; k++){
                double d_temp = GRD_dist(Q, Q[k], X[i], M, K);		// GRD_dist()
                if (d_temp < d_near){
                    near = k;
                    d_near = d_temp;
                }
            }
            W[i] = near;
            clusters[near].push_back(i);
        }
        // compute clustering cost
//        cur_cost =  cluster_cost(X, Q, W, N, M);
//        cout<<"cur_cost = "<<cur_cost<<endl;

        // update modes
        for (int k = 0; k < K; k++)
            Q[k] = find_mode(X, clusters[k], N, M);
    }
    //
//    print_clusters(clusters);

	cur_cost = cluster_cost(X, Q, W, N, M);
//    cout<<"cur_cost = "<<cur_cost<<endl;
    //
    return W;
}

//// print average distance in each cluster
void print_stats(int** X, int N, int M){
	int min_cid = 100000000;	// min, max cluster ids
	int max_cid = 0;
	for (int i = 0; i < N; i++){
		if (min_cid > X[i][M])
			min_cid = X[i][M];
		if (max_cid < X[i][M])
			max_cid = X[i][M];
	}

	for (int c = min_cid; c <= max_cid; c++){
		vector<int> idx;
		for (int i = 0; i < N; i++)
			if (X[i][M] == c)
				idx.push_back(i);
		//
		int* q = find_mode(X, idx, N, M);
		double sum_dist = 0;
		for (int i : idx)
			sum_dist += dist(q, X[i], M);
		cout<<"cluster "<<c<<" : n_c = "<<idx.size()<<" avg.dist = "<<sum_dist/idx.size()<<endl;
	}

}

//// full distance distribution, write to .csv
void distance_distr(int** X, int N, int M, string file_name){

	int* D = new int[M+1];
	for (int j = 0; j < M+1; j++)
		D[j] = 0;

	for (int u = 0; u < N; u++)
		for (int v = u+1; v < N; v++){
			int d = dist(X[u], X[v], M);
			D[d] += 1;
		}
	// write to file
	ofstream fout(file_name, ofstream::out);
	for (int j = 0; j < M+1; j++)
		fout<<D[j]<<",";

	fout.close();
}

//// membership W, ground-truth T, both have indices in [0..K-1])
double metric_rand_index(int* T, int* W, int N){
	double ret = 0.0;

	double sum = (double)N*(N-1)/2.0;
	long long s1 = 0;
	long long s2 = 0;
	long long s3 = 0;
	long long s4 = 0;
	for (int u = 0; u < N; u++)
		for (int v = u+1; v < N; v++){
			if (T[u] == T[v]){
				if (W[u] == W[v])
					s1 += 1;
				else
					s2 += 1;
			}else{
				if (W[u] == W[v])
					s3 += 1;
				else
					s4 += 1;
			}
		}
//	cout<<"s1 = "<<s1<<endl;
//	cout<<"s2 = "<<s2<<endl;
//	cout<<"s3 = "<<s3<<endl;
//	cout<<"s4 = "<<s4<<endl;
	//
	ret = (s1 + s4)/sum;

	//
	return ret;
}

//// membership W, ground-truth T, both have indices in [0..K-1])
double metric_rand_index_fast(int* T, int* W, int N, int K){
	double ret = 0.0;

	double sum = (double)N*(N-1)/2.0;
	long long s1 = 0;
	long long s4 = 0;

	//
	int** U = new int*[K];
	for (int i = 0; i < K; i++){
		U[i] = new int[K];
		for (int j = 0; j < K; j++)
			U[i][j] = 0;
	}
	for (int u = 0; u < N; u++)
		U[T[u]][W[u]] += 1;

	int n_zero = 0;
	for (int i = 0; i < K; i++)
		for (int j = 0; j < K; j++)
			if (U[i][j] == 0)
				n_zero += 1;
//	cout<<"n_zero = "<<n_zero<<endl;
	// s1
	for (int i = 0; i < K; i++)
		for (int j = 0; j < K; j++)
			if (U[i][j] > 0)
				s1 += (U[i][j] * (U[i][j] -1))/2;
	// s4
	for (int i1 = 0; i1 < K; i1++)
		for (int i2 = 0; i2 < K; i2++)
			for (int j1 = 0; j1 < K; j1++)
				for (int j2 = 0; j2 < K; j2++)
					if (i1 != i2 && j1 != j2)
						s4 += U[i1][j1] * U[i2][j2];
	s4 = s4 / 2;


//	cout<<"s1 = "<<s1<<endl;
//	cout<<"s4 = "<<s4<<endl;

	//
	ret = (s1 + s4)/sum;

	//
	return ret;
}

//// membership W, ground-truth T, both have indices in [0..K-1])
double metric_accuracy(int* T, int* W, int N, int K){
	double ret = 0.0;

	vector<vector<double>> C(K);
	for (int i = 0; i < K; i++)
		for (int j = 0; j < K; j++)
			C[i].push_back(0.0);

	for (int i = 0; i < N; i++)
		C[T[i]][W[i]] += 1;

	// Hungarian algo
	HungarianAlgorithm HungAlgo;
	vector<int> assignment;

	double cost = HungAlgo.Solve(C, assignment, MAX_COST);

	ret = cost/N;

	//
	return ret;
}

//// membership W, ground-truth T, both have indices in [0..K-1])
// return: C confusion matrix (row: predicted, col: actual)
void metric_confusion_matrix(int* T, int* W, int N, int K, int** ret, bool is_print){

	vector<vector<double>> C(K);
	for (int i = 0; i < K; i++)
		for (int j = 0; j < K; j++)
			C[i].push_back(0.0);

	for (int i = 0; i < N; i++)
		C[W[i]][T[i]] += 1;
	// DEBUG
	if (is_print){
		cout<<"C matrix"<<endl;
		for (int i = 0; i < K; i++){
			for (int j = 0; j < K; j++)
				cout<<C[i][j]<<"\t";
			cout<<endl;
		}
	}
	// Hungarian algo
	HungarianAlgorithm HungAlgo;
	vector<int> assignment;

	double cost = HungAlgo.Solve(C, assignment, MAX_COST);
	if (is_print){
		cout<<"assignment : ";
		for (int i = 0; i < K; i++)
			cout<<assignment[i]<<" ";
		cout<<endl;
	}

	for (int i = 0; i < K; i++)
		for (int j = 0; j < K; j++)
			ret[assignment[i]][j] = C[i][j];

	// DEBUG
//	cout<<"ret matrix 1"<<endl;
//	for (int i = 0; i < K; i++){
//		for (int j = 0; j < K; j++)
//			cout<<ret[i][j]<<"\t";
//		cout<<endl;
//	}


}

////
void print_matrix(int** C, int K){
	cout<<"Confusion matrix"<<endl;
	for (int i = 0; i < K; i++){
		for (int j = 0; j < K; j++)
			cout<<C[i][j]<<"\t";
		cout<<endl;
	}
}

//// input: confusion matrix C (row: predicted, col: actual)
// ret[0]: accuracy, ret[1]: precision, ret[2]: recall
vector<double> metric_accuracy_precision_recall(int** C, int N, int K){
	vector<double> ret;

	// accuracy
	double acc = 0.0;
	for (int i = 0; i < K; i++)
		acc += C[i][i];
	acc = acc / N;
	ret.push_back(acc);

	// precision
	double prec = 0.0;
	for (int i = 0; i < K; i++){
		double s = 0;
		for (int j = 0; j < K; j++)
			s += C[i][j];
		if (s == 0.0)
			prec += 1.0;
		else
			prec += C[i][i] / s;
	}
	prec = prec / K;
	ret.push_back(prec);

	// recall
	double rec = 0.0;
	for (int i = 0; i < K; i++){
		double s = 0;
		for (int j = 0; j < K; j++)
			s += C[j][i];
		if (s == 0.0)
			rec += 1.0;
		else
			rec += C[i][i] / s;
	}
	rec = rec / K;
	ret.push_back(rec);

	//
	return ret;
}

//// copied from metric_confusion_matrix()
// membership W, ground-truth T, both have indices in [0..K-1]), perm: permutation
// return: C confusion matrix (row: predicted, col: actual)
void confusion_matrix_by_permutation(int* T, int* W, int N, int K, int* perm, int** C){
	// reset C
	for (int i = 0; i < K; i++)
		for (int j = 0; j < K; j++)
			C[i][j] = 0;
	// count
	for (int i = 0; i < N; i++)
		C[perm[W[i]]][T[i]] += 1;

}

////
void find_best_permutation(int* T, int* W, int N, int K){
	int** C = new int*[K];
	for (int i = 0; i < K; i++)
		C[i] = new int[K];

	//
	int* perm = new int[K];
	for (int k = 0; k < K; k++)
		perm[k] = k;

	//
	double best_acc = 0.0;
	int* best_acc_perm = new int[K];
	double best_pr = 0.0;
	int* best_pr_perm = new int[K];
	double best_re = 0.0;
	int* best_re_perm = new int[K];
	double best_all_AC = 0.0;		// keep top tuple (AC, PR, RE)
	double best_all_PR = 0.0;
	double best_all_RE = 0.0;
	int* best_all_perm = new int[K];
	do{
		// print
//		for (int k = 0; k < K; k++)
//			cout<<perm[k]<<" ";
//		cout<<endl;
		//
		confusion_matrix_by_permutation(T, W, N, K, perm, C);

		vector<double> ret = metric_accuracy_precision_recall(C, N, K);
		// print
//		cout<<"Accuracy = "<<ret[0]<<", ";
//		cout<<"Precision = "<<ret[1]<<", ";
//		cout<<"Recall = "<<ret[2]<<endl;

		//
		if (best_acc < ret[0]){
			best_acc = ret[0];
			memcpy(best_acc_perm, perm, K*sizeof(int));
		}
		if (best_pr < ret[1]){
			best_pr = ret[1];
			memcpy(best_pr_perm, perm, K*sizeof(int));
		}
		if (best_re < ret[2]){
			best_re = ret[2];
			memcpy(best_re_perm, perm, K*sizeof(int));
		}
		//
		if (best_all_AC < ret[0]){
			best_all_AC = ret[0];
			best_all_PR = ret[1];
			best_all_RE = ret[2];
			memcpy(best_all_perm, perm, K*sizeof(int));
		}
		if ((best_all_AC == ret[0]) && (best_all_PR < ret[1])){
			best_all_PR = ret[1];
			best_all_RE = ret[2];
			memcpy(best_all_perm, perm, K*sizeof(int));
		}
		if ((best_all_AC == ret[0]) && (best_all_PR == ret[1]) && (best_all_RE < ret[2])){
			best_all_RE = ret[2];
			memcpy(best_all_perm, perm, K*sizeof(int));
		}

	}while (next_permutation(perm, perm + K));

	//
	cout<<"best_acc = "<<best_acc<<endl;
	for (int k = 0; k < K; k++)
		cout<<best_acc_perm[k]<<" ";
	cout<<endl;

	confusion_matrix_by_permutation(T, W, N, K, best_acc_perm, C);
	vector<double> ret = metric_accuracy_precision_recall(C, N, K);
	cout<<"Accuracy = "<<ret[0]<<", ";
	cout<<"Precision = "<<ret[1]<<", ";
	cout<<"Recall = "<<ret[2]<<endl;
	//
	cout<<"best_pr = "<<best_pr<<endl;
	for (int k = 0; k < K; k++)
		cout<<best_pr_perm[k]<<" ";
	cout<<endl;
	cout<<"best_re = "<<best_re<<endl;
	for (int k = 0; k < K; k++)
		cout<<best_re_perm[k]<<" ";
	cout<<endl;
	//
	cout<<"BEST Accuracy = "<<best_all_AC<<", ";
	cout<<"Precision = "<<best_all_PR<<", ";
	cout<<"Recall = "<<best_all_RE<<endl;
	cout<<"best_all_perm = ";
	for (int k = 0; k < K; k++)
		cout<<best_all_perm[k]<<" ";
	cout<<endl;
}

////
void find_nearest_cluster_GT(int** X, int** Q, int N, int M, int K){
	int** Q_GT = new int*[K];
	for (int k = 0; k < K; k++)
		Q_GT[k] = new int[M+1];

	cout<<"GT cluster size : ";
	for (int k = 0; k < K; k++){
		vector<int> idx;
		for (int i = 0; i < N; i++)
			if (X[i][M] == k)
				idx.push_back(i);
		cout<<idx.size()<<" ";
		//
		memcpy(Q_GT[k], find_mode(X, idx, N, M), M*sizeof(int));
	}
	cout<<endl;

	for (int k = 0; k < K; k++){
		int min_d = 1000000;
		vector<int> min_list;
		for (int k0 = 0; k0 < K; k0++){
			int d = dist(Q[k], Q_GT[k0], M);
			if (min_d > d){
				min_d = d;
				min_list.clear();
				min_list.push_back(k0);
			}else if (min_d == d){
				min_list.push_back(k0);
			}
		}
		//
		cout<<"GT["<<k<<"] = ";
		for (int k0 : min_list)
			cout<<k0<<" ";
		cout<<endl;
	}
}

//// for sorting in gen_FQArr()
bool compare(int** FQArr, int a, int b, int P){
	// OK
	for (int j = 0; j < P; j++){
		if (FQArr[a][j] < FQArr[b][j])
			return true;
		if (FQArr[a][j] > FQArr[b][j])
			return false;
		if (FQArr[a][j] == FQArr[b][j])
			continue;
	}

	// OK
//	if (FQArr[a][0] < FQArr[b][0])
//		return true;
//	else if (FQArr[a][0] == FQArr[b][0]){
//		if (FQArr[a][1] < FQArr[b][1])
//			return true;
//	}
	//
	return false;
}

#endif /* K_MODES_H_ */
