/*
 * k_modes_khan.cpp
 *
 *  Created on: Oct 23, 2017
 *      Author: Administrator
 *  Oct 23:
 *  	- compute metrics for kModes-init-Khan (paper: Cluster center initialization algorithm for K-modes clustering, 2013)
 */

#include <iostream>
#include <vector>

#include "helper.h"
#include "k_modes.h"

////
void compute_metrics(vector<vector<double>> &C, int N, int K){
	double sum = 0;
	for (vector<double> arr : C)
		for (double val : arr)
			sum += val;
	cout<<"sum(C) = "<<sum<<endl;

	//
	int** ret = new int*[K];
		for (int i = 0; i < K; i++)
			ret[i] = new int[K];

	// Hungarian algo
	HungarianAlgorithm HungAlgo;
	vector<int> assignment;

	double cost = HungAlgo.Solve(C, assignment, MAX_COST);
	cout<<"assignment : ";
	for (int i = 0; i < K; i++)
		cout<<assignment[i]<<" ";
	cout<<endl;

	for (int i = 0; i < K; i++)
		for (int j = 0; j < K; j++)
			ret[assignment[i]][j] = C[i][j];

	//
	print_matrix(ret, K);
	vector<double> metric = metric_accuracy_precision_recall(ret, N, K);
	cout<<"Accuracy = "<<metric[0]<<endl;
	cout<<"Precision = "<<metric[1]<<endl;
	cout<<"Recall = "<<metric[2]<<endl;
}

////
void k_modes_with_fixed_Q(int** X, int* GT, int N, int M, int K, int T, int** Q){

	cout<<"N = "<<N<<endl;
	cout<<"M = "<<M<<endl;
	cout<<"K = "<<K<<endl;
	cout<<"T = "<<T<<endl;
	// find nearest cluster id of modes in Q
	find_nearest_cluster_GT(X, Q, N, M, K);

	//
	cout<<"initial Q:"<<endl;
	print_array(Q, K, M);
	//
	int* W = k_modes(X, Q, N, M, K, T, true);

	cout<<"Q final:"<<endl;
	print_array(Q, K, M);
	//
	int** C = new int*[K];
	for (int i = 0; i < K; i++)
		C[i] = new int[K];

	metric_confusion_matrix(GT, W, N, K, C, false);
	vector<double> ret = metric_accuracy_precision_recall(C, N, K);

	cout<<"Rand Index(GT,W) = "<<metric_rand_index_fast(GT, W, N, K)<<endl;
	cout<<"Accuracy = "<<ret[0]<<endl;
	cout<<"Precision = "<<ret[1]<<endl;
	cout<<"Recall = "<<ret[2]<<endl;
}

///////////////////
int main(int argc, char* args[]) {

	// soybean
	int K = 4;
	int N = 47;
	vector<vector<double>> C;
	C.push_back(vector<double>{10,0,0,0});
	C.push_back(vector<double>{0,10,0,0});
	C.push_back(vector<double>{0,0,0,16});
	C.push_back(vector<double>{0,0,10,1});
	cout<<"soybean ------------------"<<endl;
	compute_metrics(C, N, K);

	// mushroom
	K = 2;
	N = 8124;
	C.clear();
	C.push_back(vector<double>{2573,48});
	C.push_back(vector<double>{1343,4160});
	cout<<"mushroom ------------------"<<endl;
	compute_metrics(C, N, K);

	// zoo
	K = 7;
	N = 101;
	C.clear();
	C.push_back(vector<double>{0,0,0,1,0,4,4});
	C.push_back(vector<double>{6,0,0,0,0,0,0});
	C.push_back(vector<double>{0,0,0,7,0,0,0});
	C.push_back(vector<double>{0,0,0,2,8,0,0});
	C.push_back(vector<double>{0,13,0,0,0,0,1});
	C.push_back(vector<double>{0,0,20,0,0,0,0});
	C.push_back(vector<double>{35,0,0,0,0,0,0});
	cout<<"zoo ------------------"<<endl;
	compute_metrics(C, N, K);

	// lung
	K = 3;
	N = 32;
	C.clear();
	C.push_back(vector<double>{3,7,7});
	C.push_back(vector<double>{3,4,1});
	C.push_back(vector<double>{3,2,2});
	cout<<"lung ------------------"<<endl;
	compute_metrics(C, N, K);

	// breast
	K = 2;
	N = 699;
	C.clear();
	C.push_back(vector<double>{390,189});
	C.push_back(vector<double>{68,52});
	cout<<"breast ------------------"<<endl;
	compute_metrics(C, N, K);

	// dermatology
	K = 6;
	N = 366;
	C.clear();
	C.push_back(vector<double>{0,0,0,0,0,18});
	C.push_back(vector<double>{14,2,20,9,14,0});
	C.push_back(vector<double>{46,9,43,8,22,0});
	C.push_back(vector<double>{0,95,0,0,0,0});
	C.push_back(vector<double>{0,0,0,34,0,0});
	C.push_back(vector<double>{1,6,9,1,13,2});
	cout<<"dermatology ------------------"<<endl;
	compute_metrics(C, N, K);

	// vote
	K = 2;
	N = 435;
	C.clear();
	C.push_back(vector<double>{158,55});
	C.push_back(vector<double>{10,212});
	cout<<"vote ------------------"<<endl;
	compute_metrics(C, N, K);

	// nursery
	K = 5;
	N = 12960;
	C.clear();
	C.push_back(vector<double>{1486,0,102,1263,1607});
	C.push_back(vector<double>{830,0,32,745,883});
	C.push_back(vector<double>{885,2,125,1143,500});
	C.push_back(vector<double>{465,0,15,350,565});
	C.push_back(vector<double>{654,0,54,765,489});
	cout<<"nursery ------------------"<<endl;
	compute_metrics(C, N, K);

	// chess
	K = 2;
	N = 3196;
	C.clear();
	C.push_back(vector<double>{2107,262});
	C.push_back(vector<double>{684,143});
	cout<<"chess ------------------"<<endl;
	compute_metrics(C, N, K);

	// heart
	K = 5;
	N = 303;
	C.clear();
	C.push_back(vector<double>{12,19,18,21,7});
	C.push_back(vector<double>{87,11,4,2,1});
	C.push_back(vector<double>{9,9,13,8,5});
	C.push_back(vector<double>{24,13,1,3,1});
	C.push_back(vector<double>{33,1,0,1,0});
	cout<<"heart ------------------"<<endl;
	compute_metrics(C, N, K);




	//// run my k-modes algorithm (C++) on "Computed Initial Modes" by k-modes-init-Khan
//	// soybean
//	cout<<"soybean-----------------"<<endl;
//	int N = 47;
//	int M = 21;
//	int K = 4;
//	int T = 10;
//	int** X = load_data_arff("data/", "soybean-small-mod.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	int* GT = true_membership(X, N, M);
//
//	int q_soybean[][M] = {
//	{6,0,2,1,0,1,0,1,1,2,1,0,3,1,1,1,0,0,0,0,0},
//	{6,0,0,2,1,3,3,1,1,0,1,0,0,3,0,0,0,2,1,0,0},
//	{1,1,2,0,0,2,1,2,1,1,1,0,2,2,0,0,0,0,0,3,1},
//	{0,1,2,0,0,3,1,1,0,1,0,0,1,1,0,1,1,0,0,3,0}};
//	int** Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_soybean[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);
//
//	// mushroom
//	cout<<"mushroom-----------------"<<endl;
//	N = 8124;
//	M = 22;
//	K = 2;
//	T = 10;
//	X = load_data_arff("data/", "mushroom-converted.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	GT = true_membership(X, N, M);
//
//	int q_mushroom[][M] = {
//	{3,1,3,1,4,0,0,1,5,0,2,2,3,4,4,0,0,0,2,3,4,3},
//	{0,0,0,1,3,0,0,1,8,1,2,0,0,0,0,0,0,0,0,4,3,3}};
//	Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_mushroom[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);
//
//	// zoo
//	cout<<"zoo-----------------"<<endl;
//	N = 101;
//	M = 16;
//	K = 7;
//	T = 10;
//	X = load_data_arff("data/", "zoo-converted.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	GT = true_membership(X, N, M);
//
//	int q_zoo[][M] = {
//	{0,0,0,0,0,0,1,0,0,1,1,0,8,1,0,0},
//	{0,0,0,1,0,1,1,1,1,1,0,1,0,1,0,1},
//	{0,0,1,0,0,1,1,0,0,0,0,0,6,0,0,0},
//	{0,0,1,0,1,0,0,0,0,1,0,0,6,0,0,0},
//	{0,0,1,0,0,1,1,1,1,0,0,1,0,1,0,0},
//	{0,1,1,0,1,0,0,0,1,1,0,0,2,1,0,0},
//	{1,0,0,1,0,0,1,1,1,1,0,0,4,1,0,1}};
//	Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_zoo[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);
//
//	// lung
//	cout<<"lung-----------------"<<endl;
//	N = 32;
//	M = 56;
//	K = 3;
//	T = 10;
//	X = load_data_arff("data/", "lung-cancer-converted.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	GT = true_membership(X, N, M);
//
//	int q_lung[][M] = {
//	{0,1,3,2,0,0,1,0,0,0,2,1,0,0,1,1,1,0,0,2,0,1,1,0,1,1,0,1,1,0,2,2,2,0,1,1,0,2,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,0},
//	{0,2,2,3,1,2,2,2,2,1,1,1,1,1,1,1,1,1,0,0,1,1,1,0,1,2,1,1,2,0,2,2,2,1,0,1,1,1,1,1,1,2,1,1,1,1,0,0,1,1,1,1,1,1,1,1},
//	{0,1,3,2,1,2,2,2,0,1,0,0,2,2,0,0,1,1,0,0,1,1,1,1,0,2,0,2,2,0,2,2,2,0,0,0,0,2,1,1,1,0,1,1,1,0,0,0,1,1,1,1,1,1,1,1}};
//	Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_lung[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);
//
//	// breast
//	cout<<"breast-----------------"<<endl;
//	N = 699;
//	M = 9;
//	K = 2;
//	T = 10;
//	X = load_data_arff("data/", "breast-cancer-converted.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	GT = true_membership(X, N, M);
//
//	int q_breast[][M] = {
//	{1,1,1,1,2,1,2,1,1},
//	{5,1,2,1,2,1,1,1,1}};
//	Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_breast[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);
//
//	// dermatology
//	cout<<"dermatology-----------------"<<endl;
//	N = 366;
//	M = 34;
//	K = 6;
//	T = 10;
//	X = load_data_arff("data/", "dermatology-converted.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	GT = true_membership(X, N, M);
//
//	int q_dermatology[][M] = {
//	{3,2,2,0,0,0,3,0,1,0,1,0,0,0,0,1,3,1,1,0,0,0,0,0,0,0,0,2,0,3,2,2,0,1},
//	{1,1,1,0,0,0,0,0,0,0,0,0,0,1,0,1,1,0,1,0,0,0,0,0,0,1,0,2,0,0,0,2,0,7},
//	{2,2,2,0,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,2,0,0,0,1,0,4},
//	{2,3,2,0,0,0,0,0,2,2,0,0,0,0,0,0,2,0,2,2,2,2,0,2,0,2,0,0,0,0,0,2,0,6},
//	{2,1,0,2,0,0,1,0,0,0,0,0,0,0,3,1,2,1,0,0,2,0,0,0,0,0,0,0,0,0,0,2,0,2},
//	{2,1,1,0,2,0,0,0,1,0,0,0,0,1,0,1,1,0,1,0,0,0,0,0,0,0,0,2,0,0,0,2,0,4}};
//	Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_dermatology[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);
//
//	// vote
//	cout<<"vote-----------------"<<endl;
//	N = 435;
//	M = 16;
//	K = 2;
//	T = 10;
//	X = load_data_arff("data/", "vote-converted.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	GT = true_membership(X, N, M);
//
//	int q_vote[][M] = {
//	{1,1,1,2,2,2,1,1,1,1,1,2,2,2,1,2},
//	{1,2,2,1,1,2,2,2,2,2,2,1,2,2,1,0}};
//	Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_vote[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);
//
//	// nursery
//	cout<<"nursery-----------------"<<endl;
//	N = 12960;
//	M = 8;
//	K = 5;
//	T = 10;
//	X = load_data_arff("data/", "nursery-converted.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	GT = true_membership(X, N, M);
//
//	int q_nursery[][M] = {
//	{2,4,1,0,0,1,1,2},
//	{0,4,0,0,2,1,2,2},
//	{0,1,3,0,0,0,2,2},
//	{2,4,0,0,2,1,0,2},
//	{0,2,0,3,2,0,0,2}};
//	Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_nursery[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);
//
//
//	// chess
//	cout<<"chess-----------------"<<endl;
//	N = 3196;
//	M = 36;
//	K = 2;
//	T = 10;
//	X = load_data_arff("data/", "chess-converted.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	GT = true_membership(X, N, M);
//
//	int q_chess[][M] = {
//	{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//	{0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,1}};
//	Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_chess[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);
//
//	// heart
//	cout<<"heart-----------------"<<endl;
//	N = 303;
//	M = 13;
//	K = 5;
//	T = 10;
//	X = load_data_arff("data/", "heart-converted.arff", N, M);
//	print_list(X[0], M+1);
//	print_list(X[N-1], M+1);
//	GT = true_membership(X, N, M);
//
//	int q_heart[][M] = {
//	{5,1,4,6,4,0,2,4,1,1,2,0,7},
//	{5,1,3,6,3,0,0,5,0,0,1,0,3},
//	{5,1,4,6,5,0,0,4,1,4,2,0,7},
//	{4,1,4,6,4,0,2,5,1,0,1,0,3},
//	{5,0,3,6,4,0,2,5,0,0,1,0,3}};
//	Q = new int*[K];
//	for (int k = 0; k < K; k++){
//		Q[k] = new int[M+1];
//		memcpy(Q[k], q_heart[k], M*sizeof(int));
//	}
//	k_modes_with_fixed_Q(X, GT, N, M, K, T, Q);




}


